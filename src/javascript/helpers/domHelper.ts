interface IElement{
  tagName: string,
  className?: string,
  attributes?: {
    [key:string]:string
  }
}
export function createElement({ tagName, className, attributes = {} }:IElement): Element{
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
