import { createElement } from '../../helpers/domHelper';

interface Imodal{
  title:string,
  bodyElement:Element,
  onClose:()=>void
}
export function showModal({ title, bodyElement, onClose = () => {} }:Imodal):void {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose }); 
  if (root) root.append(modal);
}

function getModalContainer():Element|null {
  return document.getElementById('root');
}

function createModal({ title, bodyElement, onClose }:Imodal):Element {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader({ title, onClose });

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader({ title, onClose }: { title: string; onClose: () => void; }):Element {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });
  
  (<HTMLElement>titleElement).innerText = title;
  (<HTMLElement>closeButton).innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal():void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
