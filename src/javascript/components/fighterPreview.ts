import { createElement } from '../helpers/domHelper';
import { IFighter } from './fighterSelector';

export function createFighterPreview(fighter:IFighter, position:string):Element {
  const positionClassName:string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement:Element = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });
  if (fighter) {
    const fName = document.createElement('h2');
    fName.innerText = `${fighter.name}`;
    fighterElement.append(fName);

    const fHealth = document.createElement('h4');
    fHealth.innerText = `Health: ${fighter.health}`;
    fighterElement.append(fHealth);

    const fAttack = document.createElement('h4');
    fAttack.innerText = `Attack: ${fighter.attack}`;
    fighterElement.append(fAttack);

    const fDefense = document.createElement('h4');
    fDefense.innerText = `Defense: ${fighter.defense}`;
    fighterElement.append(fDefense);

    const fImg = document.createElement('img');
    fImg.src = `${fighter.source}`;
    fImg.height = 300;
    fImg.alt = "fighter";
    fighterElement.append(fImg);
  }
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter:IFighter):Element {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
