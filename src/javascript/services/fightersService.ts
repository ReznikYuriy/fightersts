import { IFighter } from "../components/fighterSelector";
import { callApi } from "../helpers/apiHelper";
import { IFighterLite } from "../helpers/mockData";

class FighterService {
  async getFighters():Promise<any> {
    try {
      const endpoint = "fighters.json";
     const apiResult = await callApi(endpoint, "GET");
      console.log("APIRes",apiResult);
      return apiResult; 
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<IFighter> {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, "GET");
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
