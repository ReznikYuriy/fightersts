import { controls } from '../../constants/controls';
export async function fight(firstFighter, secondFighter) {
    return new Promise((resolve) => {
        const CRITICAL_HIT_TIMEOUT = 10000;
        const maxHealthFirstFigter = firstFighter.health;
        const maxHealthSecondFigter = secondFighter.health;
        const leftIndicator = document.getElementById("left-fighter-indicator");
        const rightIndicator = document.getElementById("right-fighter-indicator");
        let firstFighterCanAttack = true;
        let secondFighterCanAttack = true;
        let firstFighterIsDefense = false;
        let secondFighterIsDefense = false;
        let firstFighterCanSuperAttack = true;
        let secondFighterCanSuperAttack = true;
        function runTripleKeys(funcUp, ...codes) {
            let pressed = new Set();
            document.addEventListener('keydown', function (event) {
                pressed.add(event.code);
                for (let code of codes) {
                    if (!pressed.has(code)) {
                        return;
                    }
                }
                pressed.clear();
                funcUp();
            });
            document.addEventListener('keyup', function (event) {
                pressed.delete(event.code);
            });
        }
        function runOneKey(funcDown, funcUp, code) {
            document.addEventListener('keydown', function (event) {
                if (event.code == code) {
                    funcDown();
                }
            });
            document.addEventListener('keyup', function (event) {
                if (event.code == code) {
                    funcUp();
                }
            });
        }
        function attackDown(attacker, defender) {
            let damage;
            switch (attacker) {
                case firstFighter:
                    if (!firstFighterCanAttack)
                        return;
                    damage = secondFighterIsDefense ? 0 : getDamage(attacker, defender);
                    defender.health -= damage;
                    firstFighterCanAttack = false;
                    break;
                case secondFighter:
                    if (!secondFighterCanAttack)
                        return;
                    damage = firstFighterIsDefense ? 0 : getDamage(attacker, defender);
                    defender.health -= damage;
                    secondFighterCanAttack = false;
                    break;
                default:
                    break;
            }
            checkStatus();
        }
        ;
        function attackUp(attacker) {
            switch (attacker) {
                case firstFighter:
                    if (!firstFighterIsDefense)
                        firstFighterCanAttack = true;
                    break;
                case secondFighter:
                    if (!secondFighterIsDefense)
                        secondFighterCanAttack = true;
                    break;
                default:
                    break;
            }
        }
        ;
        function defenseDown(defender) {
            switch (defender) {
                case (firstFighter):
                    firstFighterIsDefense = true;
                    firstFighterCanAttack = false;
                    break;
                case (secondFighter):
                    secondFighterIsDefense = true;
                    secondFighterCanAttack = false;
                    break;
                default:
                    break;
            }
        }
        ;
        function defenseUp(defender) {
            switch (defender) {
                case (firstFighter):
                    firstFighterIsDefense = false;
                    firstFighterCanAttack = true;
                    break;
                case (secondFighter):
                    secondFighterIsDefense = false;
                    secondFighterCanAttack = true;
                    break;
                default:
                    break;
            }
        }
        ;
        function criticalHitDown(attacker, defender) {
            switch (attacker) {
                case (firstFighter):
                    if (firstFighterCanAttack && firstFighterCanSuperAttack) {
                        defender.health -= attacker.attack * 2;
                        firstFighterCanSuperAttack = false;
                        setTimeout(() => firstFighterCanSuperAttack = true, CRITICAL_HIT_TIMEOUT);
                    }
                    ;
                    break;
                case (secondFighter):
                    if (secondFighterCanAttack && secondFighterCanSuperAttack) {
                        defender.health -= attacker.attack * 2;
                        secondFighterCanSuperAttack = false;
                        setTimeout(() => secondFighterCanSuperAttack = true, CRITICAL_HIT_TIMEOUT);
                    }
                    ;
                    break;
                default:
                    break;
            }
            ;
            checkStatus();
        }
        ;
        runTripleKeys(() => { criticalHitDown(firstFighter, secondFighter); }, ...controls.PlayerOneCriticalHitCombination);
        runTripleKeys(() => { criticalHitDown(secondFighter, firstFighter); }, ...controls.PlayerTwoCriticalHitCombination);
        runOneKey(() => { attackDown(firstFighter, secondFighter); }, () => { attackUp(firstFighter); }, controls.PlayerOneAttack);
        runOneKey(() => { defenseDown(firstFighter); }, () => { defenseUp(firstFighter); }, controls.PlayerOneBlock);
        runOneKey(() => { attackDown(secondFighter, firstFighter); }, () => { attackUp(secondFighter); }, controls.PlayerTwoAttack);
        runOneKey(() => { defenseDown(secondFighter); }, () => { defenseUp(secondFighter); }, controls.PlayerTwoBlock);
        function checkStatus() {
            if (leftIndicator)
                leftIndicator.style.width = `${firstFighter.health / maxHealthFirstFigter * 100}%`;
            if (rightIndicator)
                rightIndicator.style.width = `${secondFighter.health / maxHealthSecondFigter * 100}%`;
            if (firstFighter.health <= 0 || secondFighter.health <= 0) {
                const winFighter = firstFighter.health > 0 ? firstFighter : secondFighter;
                resolve(winFighter);
            }
            ;
        }
    });
}
export function getDamage(attacker, defender) {
    let damage = getHitPower(attacker) - getBlockPower(defender);
    return damage > 0 ? damage : 0;
    // return damage
}
export function getHitPower(fighter) {
    let criticalHitChance = Math.random() + 1;
    let hitPower = fighter.attack * criticalHitChance;
    return hitPower;
    // return hit power
}
export function getBlockPower(fighter) {
    let dodgeChance = Math.random() + 1;
    let blockPower = fighter.defense * dodgeChance;
    return blockPower;
    // return block power
}
