import { createElement } from "../helpers/domHelper";
//const createElement = require('../helpers/domHelper');
import { renderArena } from "./arena";
//const renderArena = require('./arena');
//import {versusImg} from "../../../resources/";
const versusImg = require('../../../resources/versus.png');
import { createFighterPreview } from "./fighterPreview";
//const createFighterPreview=require("./fighterPreview");
import { fighterService } from "../services/fightersService";
//const fighterService=require("../services/fightersService");

export interface IFighter {
  _id: string;
  attack: number;
  defense: number;
  health: number;
  name: string;
  source: string;
}
export type SelectedFighters = [playerOne?: IFighter | undefined, playerTwo?: IFighter | null | undefined];

export function createFightersSelector() {
  let selectedFighters: SelectedFighters;
  selectedFighters = [];

  return async (event:any, fighterId: string): Promise<any> => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];
    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string): Promise<any> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  if (!fighterDetailsMap.has(fighterId)) {
    const fighter = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighter);
    return fighter;
  }
}

function renderSelectedFighters(selectedFighters: SelectedFighters): void {
  const fightersPreview = document.querySelector(
    ".preview-container___root"
  );
  const [playerOne, playerTwo] = selectedFighters;
  let firstPreview, secondPreview;
  if (playerOne) firstPreview = createFighterPreview(playerOne, "left");
  if (playerTwo) secondPreview = createFighterPreview(playerTwo, "right");
  const versusBlock = createVersusBlock(selectedFighters);
  if (fightersPreview) fightersPreview.innerHTML = "";
  if (fightersPreview&&firstPreview&&secondPreview) fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: SelectedFighters): Element {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
    const container = createElement({
    tagName: "div",
    className: "preview-container___versus-block",
  });
  const image = createElement({
    tagName: "img",
    className: "preview-container___versus-img",
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? "" : "disabled";
  const fightBtn = createElement({
    tagName: "button",
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener("click", onClick, false);
  (<HTMLElement>fightBtn).innerText = "Fight";
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: SelectedFighters): void {
  renderArena(selectedFighters);
}
