import { createElement } from "../helpers/domHelper";
import { createFighterImage } from "./fighterPreview";
import { showWinnerModal } from "./modal/winner";
import { fight } from "./fight";
import { IFighter, SelectedFighters } from "./fighterSelector";

export function renderArena(selectedFighters: SelectedFighters) {
  const root = document.getElementById("root");
  const arena = createArena(selectedFighters);

  if (root) root.innerHTML = "";
  if (root) root.append(arena);

  // todo:
  // - start the fight
  // - when fight is finished show winner
  const [playerOne, playerTwo] = selectedFighters;
  if (playerOne && playerTwo)
    fight(playerOne, playerTwo).then((res) => showWinnerModal(<IFighter>res));
}

function createArena(selectedFighters: SelectedFighters): Element {
  const arena = createElement({ tagName: "div", className: "arena___root" });
  const [playerOne, playerTwo] = selectedFighters;
  let healthIndicators, fighters;
  if (playerOne&&playerTwo) healthIndicators = createHealthIndicators(playerOne, playerTwo);
  if (playerOne&&playerTwo) fighters = createFighters(playerOne, playerTwo);

  if (healthIndicators&&fighters) arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(
  leftFighter: IFighter,
  rightFighter: IFighter
): Element {
  const healthIndicators = createElement({
    tagName: "div",
    className: "arena___fight-status",
  });
  const versusSign = createElement({
    tagName: "div",
    className: "arena___versus-sign",
  });
  const leftFighterIndicator = createHealthIndicator(leftFighter, "left");
  const rightFighterIndicator = createHealthIndicator(rightFighter, "right");

  healthIndicators.append(
    leftFighterIndicator,
    versusSign,
    rightFighterIndicator
  );
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighter, position: string): Element {
  const { name } = fighter;
  const container = createElement({
    tagName: "div",
    className: "arena___fighter-indicator",
  });
  const fighterName = createElement({
    tagName: "span",
    className: "arena___fighter-name",
  });
  const indicator = createElement({
    tagName: "div",
    className: "arena___health-indicator",
  });
  const bar = createElement({
    tagName: "div",
    className: "arena___health-bar",
    attributes: { id: `${position}-fighter-indicator` },
  });

  (<HTMLElement>fighterName).innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(
  firstFighter: IFighter,
  secondFighter: IFighter
): Element {
  const battleField = createElement({
    tagName: "div",
    className: `arena___battlefield`,
  });
  const firstFighterElement = createFighter(firstFighter, "left");
  const secondFighterElement = createFighter(secondFighter, "right");

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighter, position: string): Element {
  const imgElement = createFighterImage(fighter);
  const positionClassName =
    position === "right" ? "arena___right-fighter" : "arena___left-fighter";
  const fighterElement = createElement({
    tagName: "div",
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
