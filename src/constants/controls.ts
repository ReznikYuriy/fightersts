interface Icontrols{
  PlayerOneAttack: string,
  PlayerOneBlock: string,
  PlayerTwoAttack: string,
  PlayerTwoBlock: string,
  PlayerOneCriticalHitCombination: string[],
  PlayerTwoCriticalHitCombination: string[]
}
export const controls:Icontrols = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
}